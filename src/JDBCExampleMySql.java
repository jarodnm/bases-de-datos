import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.io.*;
import java.util.ArrayList;

public class JDBCExampleMySql {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void main (String args[]) throws java.io.IOException {
        try {
            mostrarMenu();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void mostrarMenu() throws IOException{
        int opcion;
        opcion = -1;
        do{
            out.println("1.Listar Personas");
            out.println("2.Registrar Persona");
            out.println("3.Modificar Persona ");
            out.println("4. Eliminar Persona ");
            out.println("0.Salir");
            out.println("Digite la opción que dese");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while(opcion != 0);
    }
    //Rutina para procesar la opción seleccionada por el usuario
    public static void procesarOpcion(int pOpcion)
            throws java.io.IOException{
        switch(pOpcion){
            case 1:
                listar();
                break;
            case 2:
                registrar();
                break;
            case 3:
                modificar();
                break;
            case 4:
                eliminar();
                break;
            case 0:
                out.println("Adiós");
                break;
            default:
                out.println("Opción inválida");
        }
    }
    public static void listar()
    {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            ArrayList<Persona> lista = new ArrayList<>();
            conn = DriverManager.getConnection("jdbc:sqlserver://LAB8;DatabaseName=PRUEBA;user=Jarod;password=narutobi22");
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM persona");
            while(rs.next())
            {
                Persona p = new Persona();
                p.setNombre(rs.getString("NOMBRE"));
                p.setEdad(rs.getInt("EDAD"));
                p.setApellido(rs.getString("APELLIDO"));
                lista.add(p);
            }

            //se imprime el Array list
            for(Persona p : lista){
                out.println(p.toString());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public  static void registrar()
    {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            Statement stmt = null;
            String nombre;
            String apellido;
            int edad;
            out.println("Digite el nombre de la persona");
            nombre = in.readLine();
            out.println("Digite el apellido de la persona");
            apellido = in.readLine();
            out.println("Digite la edad de la persona");
            edad = Integer.parseInt(in.readLine());
            conn = DriverManager.getConnection("jdbc:sqlserver://LAB8;DatabaseName=PRUEBA;user=Jarod;password=narutobi22");
            stmt = conn.createStatement();
            stmt.execute("INSERT INTO persona(NOMBRE,APELLIDO,EDAD) VALUES " +
                    "('"+ nombre+ "','" + apellido + "'," +
                    edad + ")");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public  static void modificar()
    {
        try {
            // The newInstance() call is a work around for someClass.forName("com.mysql.jdbc.Driver").newInstance();
            // broken Java implementations

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            Statement stmt = null;
            String nombre;
            String apellido;
            int edad;
            out.println("Digite el nombre de la persona que desea modificar");
            nombre = in.readLine();
            out.println("Digite el apellido");
            apellido = in.readLine();
            out.println("Digite la edad");
            edad = Integer.parseInt(in.readLine());
            conn = DriverManager.getConnection("jdbc:sqlserver://LAB8;DatabaseName=PRUEBA;user=Jarod;password=narutobi22");

            stmt = conn.createStatement();
            String cmdText = "UPDATE PERSONA SET EDAD = " +
                    edad + ",APELLIDO = '" + apellido + "'" +
                    " WHERE NOMBRE = '" + nombre+"'" ;
            stmt.execute(cmdText);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public  static void eliminar()
    {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            Statement stmt = null;
            String nombre;
            out.println("Digite el nombre de la persona que desea eliminar");
            nombre = in.readLine();
            conn = DriverManager.getConnection("jdbc:sqlserver://LAB8;DatabaseName=PRUEBA;user=Jarod;password=narutobi22");
            stmt = conn.createStatement();
            stmt.execute("DELETE  FROM PERSONA  WHERE NOMBRE = '" + nombre+"'");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

